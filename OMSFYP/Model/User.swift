//
//  User.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//

import Foundation

class User: Codable {
   
    
    var id: Int?
    var role: String?
    var name: String?
    var dob: String?
    var img: String?
    var gender: String?
    var address1: String?
    var address2: String?
    var phone: String?
    var email: String?
    var password: String?
    
    static func dummy() -> [User]{
        
        var users = [User]()
        
        var data = User()
        data.id = 1
        data.role = "Admin"
        data.name = "Hashim"
        data.gender = "Male"
        data.img = "hashim"
        data.address1 = "Saidpur road, Rawalpindi"
        data.address2 = "Saidpur Road, Mars"
        data.phone = "03145201562"
        data.email = "sardarhashim30@gmail.com"
        data.password = "123"
        users.append(data)
        
        data.id = 2
        data.role = "User"
        data.name = "Khan"
        data.gender = "Male"
        data.img = "hashim"
        data.address1 = "Saidpur road, Rawalpindi"
        data.address2 = "Saidpur Road, Mars"
        data.phone = "03145201562"
        data.email = "sardarhashim30@gmail.com"
        data.password = "123"
        users.append(data)
        
        data.id = 3
        data.role = "DeliveryBoy"
        data.name = "Raja"
        data.gender = "Male"
        data.img = "hashim"
        data.address1 = "Saidpur road, Rawalpindi"
        data.address2 = "Saidpur Road, Mars"
        data.phone = "03145201562"
        data.email = "sardarhashim30@gmail.com"
        data.password = "123"
        users.append(data)
        
        return users
    }
    
    
    
    
    static func save(_ data: User){
        
        UserDefaults.standard.set(codable: data, forKey: "loggedInUser")

    }
    
    
    static func delete(_ data: User){
        
        UserDefaults.standard.set(codable: User(), forKey: "loggedInUser")
        
    }
    
    
    static func get() -> User{
        
        return UserDefaults.standard.codable(User.self, forKey: "loggedInUser") ?? User()
    }

    
    
    
}
