//
//  Products.swift
//  OMSFYP
//
//  Created by shh on 16/06/2021.
//

import Foundation
struct Product {
    var id: Int?
    var company: String?
    var categoryId: String?
    var name: String?
    var imageUrl1: String?
    var imageUrl2: String?
    var imageUrl3: String?
    var quantity: String?
    var description: String?
    var type: String?
    var price: String?
    
    static func dummy() -> [Product]{
        
        var products = [Product]()
        
        var data = Product()
        data.categoryId = "Tablet"
        data.name = "Paracetamol"
        data.imageUrl1 = "para"
        data.imageUrl2 = "para"
        data.imageUrl3 = "para"
        data.quantity = "40"
        data.description = "HELLOOOOOOO"
        data.type = "Cough"
        data.price = "100"
        products.append(data)
        
        return products
    }
}
