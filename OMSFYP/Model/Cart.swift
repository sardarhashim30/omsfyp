//
//  Cart.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//

import Foundation
struct Cart {
    var cid: Int? //PRIMARY KEY
    var pid: Int?
    var uid: Int?
    var quantity: String?
    var total: Int?
    /*static func dummy() -> [Cart]{
        
        var orderdetails = [Cart]()
        
        var data = Cart()
        data.uid = "2"
        data.pid = "1"
        data.quantity = "3"
        data.total = "12299"
        orderdetails.append(data)
        
        return orderdetails
    }*/
}
