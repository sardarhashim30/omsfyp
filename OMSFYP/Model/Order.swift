//
//  Order.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//

import Foundation
struct Order {
    var oid: Int?  // PRIMARY KEY
    var uid: Int?
    var fromPrescription: Bool?
    var deliveryboyid: Int?
    var grandtotal: Int?
    var Address: String?
    var status: String?
    var time: String?
}
struct OrderDetails {
    var oid: Int? //FOREIGN KEY
    var pid: Int?
    var quantity: Int?
    var total: Int?
    
}
