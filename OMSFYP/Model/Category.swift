//
//  Category.swift
//  OMSFYP
//
//  Created by shh on 16/06/2021.
//

import Foundation
struct Category {
    var id: String?
    var name: String?
    var imageUrl: String?
    
    static func dummy() -> [Category]{
        
        var categories = [Category]()
        
        var data = Category()
        data.id = "1"
        data.name = "Tablets"
        data.imageUrl = "TABLET"
        categories.append(data)
        
        data.id = "2"
        data.name = "Inhalers"
        data.imageUrl = "INHALER"
        categories.append(data)

        
        data.id = "3"
        data.name = "Injections"
        data.imageUrl = "INJECTION"
        categories.append(data)
        
        data.id = "4"
        data.name = "Liquid"
        data.imageUrl = "LIQUID"
        categories.append(data)
        
        data.id = "4"
        data.name = "Drops"
        data.imageUrl = "drop"
        categories.append(data)
        
        data.id = "5"
        data.name = "Suppositories"
        data.imageUrl = "suppos"
        categories.append(data)
        
        data.id = "6"
        data.name = "Capsules"
        data.imageUrl = "CAPSULE"
        categories.append(data)

        
        return categories
    }
}
