//
//  DispenserProductViewController.swift
//  OMSFYP
//
//  Created by shh on 18/06/2021.
//

import UIKit

class DispenserProductViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    var Data = [Product]()

    
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var collectionViewProductList: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        self.fetchData(loadDummy: false)
    }
    func setupUI() {
        
        self.collectionViewProductList.delegate = self
        self.collectionViewProductList.dataSource = self
    }
    func fetchData(loadDummy: Bool) {
        
        if loadDummy {
            
            
        } else {
            ProductAPIManager.getProductList(dsadsa: "asdasd", completion: {
                
                data in
                self.Data = data
                
            })
        }
        self.collectionViewProductList.reloadData()
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DispenserProductCell", for: indexPath) as! DispenserProductListCell
        if(Data.count != 0){
        cell.setData(data: self.Data[indexPath.row])
        }
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width
        let height = width + 20
        return CGSize.init(width: width, height: self.view.frame.height / 5)
        
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
