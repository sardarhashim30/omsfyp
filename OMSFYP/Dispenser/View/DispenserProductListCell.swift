//
//  DispenserProductListCell.swift
//  OMSFYP
//
//  Created by shh on 18/06/2021.
//

import UIKit

class DispenserProductListCell: UICollectionViewCell {
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet var labelCategory: UILabel!
    @IBOutlet var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data: Product) {
        self.label1.text = data.name
        self.label2.text = data.type
        self.label3.text = data.description
        self.labelCategory.text = data.categoryId
        let url = URL(string: "http://10.211.55.3:2038/Uploads/" + data.imageUrl1! + ".jpeg")
        self.imageView.kf.setImage(with: url)
    }
}
