//
//  AdminUserRoleViewController.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit

class AdminUserRoleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var Data = [String]()
    var mainViewController:AdminAddUserViewController?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "" )
        cell.textLabel!.text = Data[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        mainViewController?.onUserAction(data: Data[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var RoleTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Data.append("Customer")
        Data.append("Admin")
        Data.append("Dispenser")
        Data.append("DeliveryBoy")
        
        RoleTableView.dataSource = self
        RoleTableView.delegate = self
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
