//
//  AdminUserDetailViewController.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit
import Kingfisher
class AdminUserDetailViewController: UIViewController {
    @IBOutlet weak var userimg: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblDob: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    var Data = User()
    var mainviewController: AdminUserViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        // Do any additional setup after loading the view.
    }
    func setData(){
        if(Data.img != nil){
        let url = URL(string: ProductURLS.imageBaseURL + Data.img! + ".jpeg")
        userimg.kf.setImage(with: url)
        }
        lblName.text = Data.name
        lblEmail.text = Data.email
        lblContact.text = Data.phone
        lblDob.text = Data.dob
        lblGender.text = Data.gender
        lblAddress.text = Data.address1
    }
    @IBAction func btnDelete(_ sender: UIButton) {
        UserAPIManager.DeleteUser(id: Data.id ?? 0 , completion:{ data in
                Utility.displayAlertMessage(userMessage: "User Deleted Successfully", okTapped: {} , VC: self)
        }
                                  )
        mainviewController?.fetchData(loadDummy: false)
        self.dismiss(animated: true, completion: nil  )
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
