//
//  AdminUserViewController.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit

class AdminUserViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    
    
    var Data = [User]()
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var userCollectionView: UICollectionView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.fetchData(loadDummy: false)
    }
    
    func setupUI() {
        
        self.userCollectionView.delegate = self
        self.userCollectionView.dataSource = self
    }
    
    func fetchData(loadDummy: Bool) {
        
        if loadDummy {
            
            
        } else {
            UserAPIManager.getUserList(dsadsa: "asdasd", completion: {
                
                data in
                self.Data = data
                self.userCollectionView.reloadData()
            })
        }
        self.userCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdminUserCell", for: indexPath) as! AdminUserListCell
        if(Data.count != 0){
            cell.setData(data: self.Data[indexPath.row])
        }
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width
        let height = width + 20
        return CGSize.init(width: width, height: self.view.frame.height / 5)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard.init(name: "Admin", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "AdminUserDetail") as AdminUserDetailViewController
        vc.Data = Data[indexPath.row]
        vc.mainviewController = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
}
