//
//  AdminUserListCell.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit

class AdminUserListCell: UICollectionViewCell {
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet var labelRole: UILabel!
    @IBOutlet var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data: User) {
        self.label1.text = data.name
        self.label2.text = data.phone
        self.label3.text = data.address1
        self.labelRole.text = data.role
        let url = URL(string: ProductURLS.imageBaseURL + (data.img ?? "test") + ".jpeg")
        self.imageView.kf.setImage(with: url)
    }
}
