//
//  AdminAddUserViewController.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit

class AdminAddUserViewController: UIViewController {
    @IBOutlet weak var txtName: UITextField?
    @IBOutlet weak var txtEmail: UITextField?
    @IBOutlet weak var txtPassword: UITextField?
    @IBOutlet weak var txtAddress: UITextField?
    @IBOutlet weak var txtContact: UITextField?
    @IBOutlet weak var txtGender: UITextField?
    @IBOutlet weak var btnSave: UIButton?
    @IBOutlet weak var btnRole: UIButton?
    var usrRole: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    func onUserAction(data: String)
    {
        print("Data received: \(data)")
        usrRole = data
        btnRole?.setTitle(usrRole, for: .normal)
    }
    @IBAction func btnRole(_ sender: Any){
        let sb = UIStoryboard.init(name: "Admin", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "adminusercategory") as AdminUserRoleViewController
        vc.mainViewController = self
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnSave(_ sender: Any){
        var data = User()
        data.id = nil
        data.address1 = txtAddress?.text
        data.gender = txtGender?.text
        data.email = txtEmail?.text
        data.name = txtName?.text
        data.password = txtPassword?.text
        data.phone = txtContact?.text
        data.role = usrRole
        UserAPIManager.registration(User: data, completion: {
            
            data in
            if(data == true)
            {
                Utility.displayAlertMessage(userMessage: "User Added Successfully ", okTapped: {
                    
                },VC: self)
            }
            
            
            else {
                Utility.displayAlertMessage(userMessage: "Failed to Add User" , okTapped: {
                    
                },VC: self)
                
            }
        })
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
