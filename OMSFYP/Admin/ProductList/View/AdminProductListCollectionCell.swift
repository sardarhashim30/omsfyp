//
//  AdminProductListCollectionCell.swift
//  OMSFYP
//
//  Created by mac on 17/06/2021.
//

import UIKit
import Kingfisher
class AdminProductListCollectionCell: UICollectionViewCell {
    
    @IBOutlet var label1: UILabel!
    @IBOutlet var label2: UILabel!
    @IBOutlet var label3: UILabel!
    @IBOutlet var labelCategory: UILabel!
    @IBOutlet var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data: Product) {
        self.label1.text = data.name
        self.label2.text = data.type
        self.label3.text = data.description
        self.labelCategory.text = data.categoryId
        if(data.imageUrl1 != nil)
        {
            let url = URL(string: ProductURLS.imageBaseURL + data.imageUrl1!)
            self.imageView.kf.setImage(with: url)
        }
    }
    
}
