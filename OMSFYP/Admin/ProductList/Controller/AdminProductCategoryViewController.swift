//
//  AdminProductCategoryViewController.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit

class AdminProductCategoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var Data = [String]()
    var mainViewController:AdminProductAddVCViewController?
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "" )
        cell.textLabel!.text = Data[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        mainViewController?.onUserAction(data: Data[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var CategoryTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Data.append("Tablet")
        Data.append("Liquid")
        Data.append("Inhalers")
        Data.append("Injections")
        Data.append("Capsules")
        Data.append("Drops")
        Data.append("Suppository")
        
        
        
        CategoryTableView.dataSource = self
        CategoryTableView.delegate = self
        // Do any additional setup after loading the view.
    }
}
