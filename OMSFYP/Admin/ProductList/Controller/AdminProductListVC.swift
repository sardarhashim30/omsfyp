//
//  AdminProductListVC.swift
//  OMSFYP
//
//  Created by mac on 17/06/2021.
//

import UIKit

class AdminProductListVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    
    
   
    var Data = [Product]()
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var productCollectionView: UICollectionView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.fetchData(loadDummy: false)
    }
    func reload(){
        self.fetchData(loadDummy: false)
    }
    func setupUI() {
        
        self.productCollectionView.delegate = self
        self.productCollectionView.dataSource = self
    }
    
    func fetchData(loadDummy: Bool) {
        
        if loadDummy {
            
            
        } else {
            ProductAPIManager.getProductList(dsadsa: "asdasd", completion: {
                
                data in
                self.Data = data
                self.productCollectionView.reloadData()
            })
        }
        self.productCollectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard.init(name: "Admin", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "AdminProductDetail") as AdminProductDetailViewController
        vc.data = Data[indexPath.row]
        vc.mainViewController = self
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdminProductCell", for: indexPath) as! AdminProductListCollectionCell
        if(Data.count != 0){
            cell.setData(data: self.Data[indexPath.row])
        }
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.view.frame.width
        let height = width + 20
        return CGSize.init(width: width, height: self.view.frame.height / 5)
        
    }
    
    
}
