//
//  AdminProductDetailViewController.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit

class AdminProductDetailViewController: UIViewController {
    var mainViewController:AdminProductListVC?
    var data: Product?
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDescription: UITextView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        // Do any additional setup after loading the view.
    }
    
    func setupData() {
        
        if let data = data {
            lblName.text = data.name ?? ""
            lblType.text = data.company ?? ""
            lblDescription.text = data.description ?? ""
            lblCategory.text = data.categoryId ?? ""
            if let url = data.imageUrl1 {
                self.img1.kf.setImage(with: URL(string: ProductURLS.imageBaseURL + url))
            }
            if let url = data.imageUrl2 {
                self.img2.kf.setImage(with: URL(string: ProductURLS.imageBaseURL + url))
            }
            if let url = data.imageUrl3 {
                self.img3.kf.setImage(with: URL(string: ProductURLS.imageBaseURL + url))
            }
        } else {
            // no data is set
        }
    }
    @IBAction func btnDelete(_ sender: Any) {
        ProductAPIManager.DeleteProduct(id: data?.id ?? 0 , completion:{ data in
            
                Utility.displayAlertMessage(userMessage: "Product Deleted Successfully", okTapped: {} , VC: self)
            
        }
                                  )
        mainViewController?.fetchData(loadDummy: false)
        self.dismiss(animated: true, completion: nil  )
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
