//
//  AdminProductAddVCViewController.swift
//  OMSFYP
//
//  Created by mac on 17/06/2021.
//

import UIKit

class AdminProductAddVCViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtType: UITextField!
    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtQuantity: UITextField!
    @IBOutlet weak var txtCompany: UITextField!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!
    var image1 = false
    var image2 = false
    var image3 = false
    var category: String?
    @IBOutlet weak var btnCat: UIButton!
    @IBAction func imG1(_ sender: UIButton) {
        image1 = true
        var mypickercontroller = UIImagePickerController()
        mypickercontroller.delegate = self
        mypickercontroller.sourceType = .photoLibrary
        self.present(mypickercontroller, animated: true, completion: nil)
    }
    @IBAction func imG2(_ sender: UIButton) {
        image2 = true
        var mypickercontroller = UIImagePickerController()
        mypickercontroller.delegate = self
        mypickercontroller.sourceType = .photoLibrary
        self.present(mypickercontroller, animated: true, completion: nil)
    }
    @IBAction func imG3(_ sender: UIButton) {
        image3 = true
        var mypickercontroller = UIImagePickerController()
        mypickercontroller.delegate = self
        mypickercontroller.sourceType = .photoLibrary
        self.present(mypickercontroller, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if(image1 == true){
                self.img1.image = image}
            if(image2 == true){
                self.img2.image = image}
            if(image3 == true){
                self.img3.image = image}
            
            let imgData = image.jpegData(
                compressionQuality: 0.2)!
            ImageAPIManager.UploadImage(imgData: imgData, completion: {
                
                data in
                if(data == true)
                {
                }
            })
            
            
            
        }else{
            debugPrint("Something went wrong")
        }
        image1 = false
        image2 = false
        image3 = false
        self.dismiss(animated: true, completion: nil)
    }
    func onUserAction(data: String)
    {
        print("Data received: \(data)")
        category = data
        btnCat?.setTitle(category, for: .normal)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnCat(_ sender: Any){
        let sb = UIStoryboard.init(name: "Admin", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "AdminProductCategory") as AdminProductCategoryViewController
        vc.mainViewController = self
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnSave(_ sender: Any){
        let imgData1 = self.img1.image?.jpegData(
            compressionQuality: 0.2)!
        let imgData2 = self.img2.image?.jpegData(
            compressionQuality: 0.2)!
        let imgData3 = self.img3.image?.jpegData(
            compressionQuality: 0.2)!
        var Data = Product()
        Data.imageUrl1 = imgData1?.description
        Data.imageUrl2 = imgData2?.description
        Data.imageUrl3 = imgData3?.description
        Data.imageUrl1 = Data.imageUrl1?.components(separatedBy: " ").first ?? "" + ".jpeg"
        Data.imageUrl2 = Data.imageUrl2?.components(separatedBy: " ").first ?? "" + ".jpeg"
        Data.imageUrl3 = Data.imageUrl3?.components(separatedBy: " ").first ?? "" + ".jpeg"
        Data.imageUrl1 = Data.imageUrl1! + ".jpeg"
        Data.imageUrl2 = Data.imageUrl2! + ".jpeg"
        Data.imageUrl3 = Data.imageUrl3! + ".jpeg"
        Data.price = txtPrice.text
        Data.company = txtCompany.text
        Data.name = txtName.text
        Data.type = txtType.text
        Data.quantity = txtQuantity.text
        Data.categoryId = category
        ProductAPIManager.newproduct(Data: Data, completion: {
            
            data in
            if(data == true)
            {
                Utility.displayAlertMessage(userMessage: "Successfully Added", okTapped: { }, VC: self)
            }
        })
        }


/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */

}
