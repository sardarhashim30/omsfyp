//
//  RecentlyAddedProductCollectionViewCell.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit
import Kingfisher
class RecentlyAddedProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: Product) {
        self.lblName.text = data.name
        self.lblPrice.text = data.price
        if(data.imageUrl1 != nil){
        let url = URL(string: ProductURLS.imageBaseURL + data.imageUrl1!)
        self.imageView.kf.setImage(with: url)
        }
    }
}

