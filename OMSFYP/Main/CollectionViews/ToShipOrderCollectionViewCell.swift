//
//  ToShipOrderCollectionViewCell.swift
//  OMSFYP
//
//  Created by shh on 20/06/2021.
//

import UIKit

class ToShipOrderCollectionViewCell: UICollectionViewCell {
    @IBOutlet var lblOrderID: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var lblStatus: UILabel!
    @IBOutlet var HiddenID: UILabel!
    var mainviewcontroller: ToShipOrderViewController?
    var dt = Order()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: Order) {
        self.lblTotal.text = data.grandtotal?.description
        self.lblOrderID.text = data.oid?.description
        self.lblStatus.text = data.status?.description
        self.HiddenID.text = data.oid?.description
    }
    @IBAction func btnCancel(_ sender: Any) {
        var Status: String?
        Status = "Canceled"
        OrderApiManager.ChangeStatus(id: Int(HiddenID.text ?? "") ?? 0, status: Status ?? "", completion: {
            data in
            if(data != nil)
            {
                self.dt = data!
                self.mainviewcontroller?.fetchData()
            }
        })
    }
    
    
    
}
