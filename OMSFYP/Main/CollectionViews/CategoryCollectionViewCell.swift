//
//  CategoryCollectionViewCell.swift
//  OMSFYP
//
//  Created by shh on 16/06/2021.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(data: Category) {
        self.label.text = data.name
        self.imageView.image = UIImage.init(named: data.imageUrl!)
    }
}
