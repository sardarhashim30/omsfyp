//
//  OrderDetailListCollectionViewCell.swift
//  OMSFYP
//
//  Created by shh on 21/06/2021.
//

import UIKit

class OrderDetailListCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    // storyboard reference OrderDetailListCollectionCell
    func setData(Data: OrderDetails, pData: Product)
    {
        lblName.text = pData.name?.description
        lblPrice.text = "PKR: " + (pData.price?.description ?? "")
        lblQuantity.text = "x " + (Data.quantity?.description ?? "")
        lblTotal.text = Data.total?.description
        if(pData.imageUrl1 != nil){
            let url = URL(string: ProductURLS.imageBaseURL + pData.imageUrl1!)
            self.imgProduct.kf.setImage(with: url)
        }
    }
}
