//
//  CartAddressChangeViewController.swift
//  OMSFYP
//
//  Created by mac on 19/06/2021.
//

import UIKit

class CartAddressChangeViewController: UIViewController  {
    var mainViewController:MainCartViewController?
    
    @IBOutlet var Address: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Address.text = mainViewController?.lblAddress.text
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnChangeAddress(_ sender: Any) {
        mainViewController?.onUserAction(data: Address.text)
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
