//
//  MainCartCollectionViewCell.swift
//  OMSFYP
//
//  Created by mac on 19/06/2021.
//

import UIKit

class MainCartCollectionViewCell: UICollectionViewCell, testDelegate {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblType: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var txtQuantity: UITextField!
    @IBOutlet var stpQuantity: UIStepper!
    @IBOutlet var lblID: UILabel!
    var mainViewController:MainCartViewController?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func onUserAction(data: String) {
        
    }
    @IBAction func stpQuantityChanged(_ sender: Any) {
        var beforevalue: Int
        var aftervalue: Int
        var grandtotal: Int
        var singleValue: Int
        beforevalue = Int(lblTotal.text ?? "") ?? 0
        grandtotal = Int(mainViewController?.lblGrandTotal.text ?? "") ?? 0
        grandtotal = grandtotal - beforevalue
        singleValue = (Int(lblTotal.text ?? "") ?? 0) / (Int(txtQuantity.text ?? "") ?? 0)
        txtQuantity.text = Int(self.stpQuantity.value).description
        aftervalue = singleValue * (Int(txtQuantity.text ?? "") ?? 0)
        grandtotal = grandtotal + aftervalue
        lblTotal.text = aftervalue.description
        mainViewController?.onUserAction(data: String(grandtotal) )
        
    }
    @IBAction func btnRemove(_ sender: Any) {
        CartApiManager.DeleteCart(id: (Int(lblID.text ?? "") ?? 0), completion: {
            data in
            self.mainViewController?.fetchData()
        })
    }
    
    func setData(data: Product,cdata: Cart) {
        self.stpQuantity.value += (Double(cdata.quantity ?? "") ?? 0)
        self.lblName.text = data.name
        self.lblTotal.text = cdata.total?.description
        self.lblType.text = data.categoryId
        self.lblID.text = cdata.cid?.description
        self.txtQuantity.text = cdata.quantity
        if(data.imageUrl1 != nil){
        let url = URL(string: ProductURLS.imageBaseURL + data.imageUrl1!)
        self.imageView.kf.setImage(with: url)
        }
    }
    
}
