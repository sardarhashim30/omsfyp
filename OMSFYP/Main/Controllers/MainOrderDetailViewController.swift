//
//  MainOrderDetailViewController.swift
//  OMSFYP
//
//  Created by shh on 21/06/2021.
//

import UIKit

class MainOrderDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    var Data = Order()
    var odData = [OrderDetails]()
    var DBData = User()
    var pData = [Product]()
    @IBOutlet weak var lblCustomerName: UILabel!
    @IBOutlet weak var lblCustomerContact: UILabel!
    @IBOutlet weak var lblCustomerAddress: UILabel!
    @IBOutlet weak var lblDBName: UILabel!
    @IBOutlet weak var lblDBContact: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblOrderTime: UILabel!
    @IBOutlet weak var lblGrandTotal: UILabel!
    @IBOutlet var OrderItemsCollectionCell: UICollectionView!
    @IBOutlet weak var DBImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.OrderItemsCollectionCell.delegate = self
        self.OrderItemsCollectionCell.dataSource = self
        self.fetchData()
        self.setData()
        // Do any additional setup after loading the view.
    }
    
    func fetchData()
    {
        if(Appstatemanager.shared.isLoggedIn)
        {
            //CALL API AND GET DELIVERY BOY DATA AND SET DELIVERY BOY DATA
            UserAPIManager.getSingleUser(UID: Data.deliveryboyid ?? 0, completion: {
                data in
                if(data != nil)
                {
                    self.DBData = data!
                }
            })
            do{
                sleep(1)
            }
            //CALL API AND GET ORDERDETAILS DATA AND SET COLLECTION CELL DATA
            OrderApiManager.getOrderItemList(ID: Data.oid ?? 0, single: true, completion: {
                data in
                if(data != nil)
                {
                    self.odData = data
                    
                }
            })
            do{
                sleep(1)
            }
            ProductAPIManager.getProductList(dsadsa: "", completion: {
                data in
                if(data != nil)
                {
                    self.pData = data
                    self.OrderItemsCollectionCell.reloadData()
                }
            })
            do{
                sleep(1)
            }
            
        }
    }
    func setData()
    {
        lblCustomerName.text = Appstatemanager.shared.loggedInUser.name?.description
        lblCustomerContact.text = Appstatemanager.shared.loggedInUser.phone?.description
        lblCustomerAddress.text = Appstatemanager.shared.loggedInUser.address1?.description
        lblDBName.text = DBData.name?.description
        lblDBContact.text = DBData.phone?.description
        lblOrderID.text = "Order ID: " + (Data.oid?.description ?? "")
        lblGrandTotal.text = Data.grandtotal?.description
        lblOrderTime.text = "Placed On: " + (Data.time?.description ?? "")
        if(DBData.img != nil){
            let url = URL(string: ProductURLS.imageBaseURL + DBData.img!)
            self.DBImage.kf.setImage(with: url)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.odData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderDetailListCollectionCell", for: indexPath) as! OrderDetailListCollectionViewCell
        var temp = Product()
        temp.id = odData[indexPath.row].pid
        for item in pData{
            if(item.id == temp.id){
                temp = item
            }
        }
        cell.setData(Data: self.odData[indexPath.row],pData: temp)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let width = self.view.frame.width - 0
        let height: CGFloat = 140
        return CGSize.init(width: width, height: height)
        
    }
}
