//
//  LoginViewController.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    
    @IBOutlet weak var imageViewLoged: UIImageView!
    @IBOutlet weak var labelNameLoged: UILabel!
    @IBOutlet weak var buttonLogout: UIButton!
    @IBOutlet weak var viewLoged: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
       
        if Appstatemanager.shared.isLoggedIn {
            viewLoged.isHidden = false
            labelNameLoged.text = Appstatemanager.shared.loggedInUser.name
            
        } else {
            viewLoged.isHidden = true
            
        }
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        Appstatemanager.shared.isLoggedIn = false
        Appstatemanager.shared.loggedInUser = User()
        self.tabBarController?.selectedIndex = 0

    }
    
    @IBAction func btnLogin(_ sender: Any){
        UserAPIManager.loginValidator(email: txtEmail.text ?? "", password: txtPassword.text ?? "", completion: {
            
            data in
            
            if let data = data {
                if(data.id != 0){
                    Appstatemanager.shared.isLoggedIn = true
                    Appstatemanager.shared.loggedInUser = data
                    Utility.displayAlertMessage(userMessage: "Logged In Successfully", okTapped: {
                        self.tabBarController?.selectedIndex = 0
                    }, VC: self)
                }
                else {
                    
                    Utility.displayAlertMessage(userMessage: "Invalid Username/Password", okTapped: {
                        // do nothing
                    }, VC: self)
                }
            }
            else {
                
                Utility.displayAlertMessage(userMessage: "Invalid Username/Password", okTapped: {
                    // do nothing
                }, VC: self)
            }
            
        })



    }
}
