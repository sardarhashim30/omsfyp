//
//  MainProductDetailViewController.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit
import ImageSlideshow
import Kingfisher
class MainProductDetailViewController: UIViewController {
    //@IBOutlet var imgView1: UIImageView!
    //@IBOutlet var imgView2: UIImageView!
    //@IBOutlet var imgView3: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDescription: UITextView!
    @IBOutlet var txtQuantity: UITextField!
    @IBOutlet var stpQuantity: UIStepper!
        @IBOutlet weak var slideShow: ImageSlideshow!
    var Data = Product()
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func stpValueChanged(_ sender: Any) {
        txtQuantity.text = Int(self.stpQuantity.value).description
        
    }
    func fetchData()
    {
        var img1 = UIImageView()
        var img2 = UIImageView()
        var img3 = UIImageView()
        if(Data.imageUrl1 != nil){
            img1.kf.setImage(with: URL(string: ProductURLS.imageBaseURL + self.Data.imageUrl1!))
        }
        if(Data.imageUrl2 != nil) {
            img2.kf.setImage(with: URL(string: ProductURLS.imageBaseURL + self.Data.imageUrl2!))
        }
        if(Data.imageUrl2 != nil) {
            img3.kf.setImage(with: URL(string: ProductURLS.imageBaseURL + self.Data.imageUrl3!))
        }
        
        slideShow.setImageInputs([
            ImageSource(image: (img1.image ?? UIImage.init(named: "pill_placeholder"))!),
            ImageSource(image: (img2.image ?? UIImage.init(named: "pill_placeholder"))!),
            ImageSource(image: (img3.image ?? UIImage.init(named: "pill_placeholder"))!)
        ])
        lblName.text = Data.name
        lblDescription.text = Data.description
        lblPrice.text = Data.price
        txtQuantity.text = "1"
        
    }
    @IBAction func btnCart(_ sender: Any) {
        if(Appstatemanager.shared.isLoggedIn) //USERLOGGED IN
        {
            var cData = Cart()
            cData.pid = Data.id
            cData.uid = Appstatemanager.shared.loggedInUser!.id!  ///NEEEEEEDDD USER IDDDDDDDD
            cData.quantity = txtQuantity.text
            cData.total = (Int(lblPrice.text ?? "") ?? 0) * (Int(txtQuantity.text ?? "") ?? 0)
            CartApiManager.AddCart(Data: cData, completion: {
                data in
                Utility.displayAlertMessage(userMessage: "Data Added to Cart", okTapped: {}, VC: self)
            })
        }
        else{
            Utility.displayAlertMessage(userMessage: "You Need To be Logged in to Add Items In Cart", okTapped: {
                
            }, button2Tapped: {
                
                self.tabBarController?.selectedIndex = 3
            }, VC: self)

        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
