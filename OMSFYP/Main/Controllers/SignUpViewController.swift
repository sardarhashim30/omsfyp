//
//  SignUpViewController.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//

import UIKit

class SignUpViewController: UIViewController, UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    var imagename: String!
    @IBOutlet weak var DOBPicker: UIDatePicker!
    @IBOutlet weak var userIMG: UIImageView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func imG(_ sender: UIButton) {
        var mypickercontroller = UIImagePickerController()
        mypickercontroller.delegate = self
        mypickercontroller.sourceType = .photoLibrary
        self.present(mypickercontroller, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.userIMG.image = image
            let imgData = image.jpegData(
                compressionQuality: 0.2)!
            imagename = imgData.description
            ImageAPIManager.UploadImage(imgData: imgData, completion: {
                
                data in
                if(data == true)
                {
                }
            })
            
            
            
        }else{
            debugPrint("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnSignup(_ sender: Any){
        DOBPicker.datePickerMode = UIDatePicker.Mode.date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd yyyy"
        let selectedDate = dateFormatter.string(from: DOBPicker.date)
        print("selectedDate",selectedDate)
        var data = User()
        data.id = nil
        data.address1 = txtAddress.text
        data.dob = selectedDate
        print(data.dob)
        data.gender = txtGender.text
        data.email = txtEmail.text
        if imagename != nil {
            data.img = imagename.components(separatedBy: " ").first ?? "" + ".jpeg"
            data.img = data.img! + ".jpeg"
        }
        data.name = txtName.text
        data.password = txtPassword.text
        data.phone = txtContact.text
        UserAPIManager.registration(User: data, completion: {
            
            data in
            if(data == true)
            {
                Utility.displayAlertMessage(userMessage: "Registration Successful. You Can Login Now. ", okTapped: {
                    
                },VC: self)
            }
            
            
            else {
                Utility.displayAlertMessage(userMessage: "Failed to Register. Please Try Again Later" , okTapped: {
                    
                },VC: self)
                
            }
        })
        
    }
    
}

/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destination.
 // Pass the selected object to the new view controller.
 }
 */


