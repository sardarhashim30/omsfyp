//
//  MainProductListingViewController.swift
//  OMSFYP
//
//  Created by shh on 16/06/2021.
//

import UIKit

class MainProductListingViewController: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    @IBOutlet weak var RecentlyAddedItem: UICollectionView!
    var categoriesData = [Category]()
    var Data = [Product]()
    var recentData = [Product]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchData(loadDummy: false)
    }
    func setupUI() {

        self.collectionViewCategories.delegate = self
        self.collectionViewCategories.dataSource = self
        self.RecentlyAddedItem.delegate = self
        self.RecentlyAddedItem.dataSource = self
    }
    
    func fetchData(loadDummy: Bool) {
        
        if loadDummy {
            
            
        } else {
            self.categoriesData = Category.dummy()
            ProductAPIManager.getProductList(dsadsa: "asdasd", completion: {
                
                data in
                self.Data = data
                self.recentData = self.Data.reversed()
                self.RecentlyAddedItem.reloadData()
                self.collectionViewCategories.reloadData()
            })
        }
        self.RecentlyAddedItem.reloadData()
        self.collectionViewCategories.reloadData()
    }
    @IBAction func btnAll(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "ProductListAllView") as ProductListAllViewController
        vc.Data = self.Data
        
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MainProductListingViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(self.collectionViewCategories == collectionView){
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollectionCell", for: indexPath) as! CategoryCollectionViewCell
        cell.setData(data: self.categoriesData[indexPath.row])
        return cell
        }
        if(self.RecentlyAddedItem == collectionView)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentlyAddedProduct", for: indexPath) as! RecentlyAddedProductCollectionViewCell
            cell.setData(data: self.recentData[indexPath.row])
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecentlyAddedProduct", for: indexPath) as! RecentlyAddedProductCollectionViewCell
        cell.setData(data: self.recentData[indexPath.row])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(self.collectionViewCategories == collectionView)
        {
            return categoriesData.count
        }
        
        if(self.RecentlyAddedItem == collectionView)
        {
            if self.recentData.count > 10 {
                return 10
            } else {
                return self.recentData.count
            }
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.collectionViewCategories == collectionView)
        {
            let sb = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(identifier: "ProductListByCategoryView") as ProductListbyCategoryViewController
            vc.Data = self.Data
            vc.category = self.categoriesData[indexPath.row].name
            
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        if (self.RecentlyAddedItem == collectionView)
        {
            let sb = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(identifier: "MainProductDetail") as MainProductDetailViewController
            vc.Data = self.Data[indexPath.row]
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
extension MainProductListingViewController: UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collectionViewCategories {
            let width = self.view.frame.width / 4
            let height = width + 20
            return CGSize.init(width: width, height: height)
        } else {
            let width = self.view.frame.width / 2.5
            let height = width + 30
            return CGSize.init(width: width, height: height)
        }
    }
}

