//
//  MainCartViewController.swift
//  OMSFYP
//
//  Created by mac on 19/06/2021.
//

import UIKit
protocol testDelegate{
    func onUserAction(data: String)
}
class MainCartViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var Data = [Cart]()
    var pData = [Product]()
    var selectedData = [Product]()
    var category: String?
    var gt = Int()
    @IBOutlet var CartCollectionView: UICollectionView!
    @IBOutlet var lblGrandTotal: UILabel!
    @IBOutlet var lblAddress: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        CartCollectionView.delegate = self
        CartCollectionView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if Appstatemanager.shared.isLoggedIn {
            fetchData()
        }
    }
    
    func fetchData()
    {
        lblAddress.text = Appstatemanager.shared.loggedInUser?.address1
        ProductAPIManager.getProductList(dsadsa: "asdasd", completion: {
            
            data in
            self.pData = data
            self.CartCollectionView.reloadData()
        })
        if(Appstatemanager.shared.loggedInUser?.id != nil){
            CartApiManager.GetCartList(userID: (Appstatemanager.shared.loggedInUser?.id)! , completion: {
                data in
                self.Data = data
                self.gt = 0
                for item in self.Data{
                    self.gt = self.gt + (item.total ?? 0)
                }
                self.lblGrandTotal.text = self.gt.description
                self.CartCollectionView.reloadData()
            })
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.Data.count == 0 {
            var noDataLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: collectionView.bounds.size.width, height: collectionView.bounds.size.height))
        noDataLabel.text = "No product(s) in the cart"
        noDataLabel.textColor = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
        noDataLabel.textAlignment = NSTextAlignment.center
            collectionView.backgroundView = noDataLabel
        } else {
            collectionView.backgroundView = UIView()
        }
        
        return self.Data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CartCollectionView", for: indexPath) as! MainCartCollectionViewCell
        cell.mainViewController = self
        var temp = Product()
        temp.id = Data[indexPath.row].pid
        for item in pData{
            if(item.id == temp.id){
                temp = item
            }
        }
        cell.setData(data: temp,cdata: Data[indexPath.row] )
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let width = self.view.frame.width - 0
        let height: CGFloat = 140
        return CGSize.init(width: width, height: height)
        
    }
    func onUserAction(data: String)
    {
        print("Data received: \(data)")
        lblGrandTotal.text = data
    }
    @IBAction func btnChangeAddress(_ sender: Any) {
        if(Appstatemanager.shared.isLoggedIn)
        {
            let sb = UIStoryboard.init(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(identifier: "CartAddressChange") as CartAddressChangeViewController
            vc.mainViewController = self
            self.present(vc, animated: true, completion: nil)
        } 
    }
    @IBAction func btnOrderPlace(_ sender: Any) {
        if(Appstatemanager.shared.isLoggedIn)
        {
            var odata = Order()
            var oid = Int()
            var odetail = OrderDetails()
            odata.uid = Appstatemanager.shared.loggedInUser?.id
            odata.grandtotal = Int(lblGrandTotal.text ?? "")
            
            OrderApiManager.AddOrder(Data: odata, completion: {
                data in
                if(data == true)
                {
                }
            })
            do{
                sleep(3)
            }
            OrderApiManager.GetOrderId(Data: odata.uid ?? 0, completion: {
                data in
                
                if(data != nil)
                {
                    
                    oid = data ?? 0
                    for item in self.Data{
                        do{
                            sleep(3)
                        }
                        odetail.oid = oid
                        odetail.pid = item.pid
                        odetail.quantity = Int(item.quantity ?? "") ?? 0
                        odetail.total = item.total
                        OrderApiManager.AddOrderDetails(data: odetail, completion: {
                            data in
                            if(data == true){
                                
                            }
                        })
                    }
                }
            })
            self.fetchData()
            Utility.displayAlertMessage(userMessage: "Order Placed Successfully. You Can View You can view Orders in Orders Menu", okTapped: {}, VC: self)
            
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
