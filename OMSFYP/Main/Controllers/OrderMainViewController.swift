//
//  OrderMainViewController.swift
//  OMSFYP
//
//  Created by shh on 21/06/2021.
//

import UIKit

class OrderMainViewController: UIViewController {
    var Data = [Order]()
    override func viewDidLoad() {
        super.viewDidLoad()
        OrderApiManager.getOrderSingleList(ID: Appstatemanager.shared.loggedInUser.id ?? 0, completion: {
            data in
            if(data != nil){
                self.Data = data
            }
        })
        
    }
    @IBAction func btnToPay(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "ToPayOrderView") as ToPayOrderViewController
        vc.Data = self.Data
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnToShip(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "ToShipOrderView") as ToShipOrderViewController
        vc.Data = self.Data
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnToDeliver(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "ToRecieveOrderView") as ToRecieveOrderViewController
        vc.Data = self.Data
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnToReview(_ sender: Any) {
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "ToReviewOrderView") as ToReviewOrderViewController
        vc.Data = self.Data
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
