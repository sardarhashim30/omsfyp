//
//  ProductListbyCategoryViewController.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import UIKit

class ProductListbyCategoryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var selectedData = [Product]()
    var Data = [Product]()
    var category: String?
    @IBOutlet var ProductCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ProductCollectionView.delegate = self
        ProductCollectionView.dataSource = self
        fetchData()
        // Do any additional setup after loading the view.
    }
    func fetchData()
    {
        for item in Data
        {
            if(item.categoryId == category)
            {
                selectedData.append(item)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductListByCategoryCell", for: indexPath) as! ProductListbyCategoryCollectionViewCell
        cell.setData(data: self.selectedData[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //MainProductDetail
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "MainProductDetail") as MainProductDetailViewController
        vc.Data = self.selectedData[indexPath.row]
        
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let width = self.view.frame.width - 0
        let height: CGFloat = 140
        return CGSize.init(width: width, height: height)
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
