//
//  ToRecieveOrderViewController.swift
//  OMSFYP
//
//  Created by shh on 20/06/2021.
//

import UIKit

class ToRecieveOrderViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    var selectedData = [Order]()
    var Data = [Order]()
    var category: String?
    @IBOutlet var OrderCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        OrderCollectionView.delegate = self
        OrderCollectionView.dataSource = self
        fetchData()
        setData()
        // Do any additional setup after loading the view.
    }
    func fetchData()
    {
        for item in Data
        {
            if(item.status == "Shipped")
            {
                selectedData.append(item)
            }
        }
    }
    func setData()
    {
        OrderCollectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.selectedData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ToRecieveCollectionCell", for: indexPath) as! ToRecieveOrderCollectionViewCell
        cell.setData(data: self.selectedData[indexPath.row])
        cell.mainviewcontroller = self
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //MainProductDetail
        let sb = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(identifier: "MainOrderDetail") as MainOrderDetailViewController
        vc.Data = self.Data[indexPath.row]
        
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let width = self.view.frame.width - 0
        let height: CGFloat = 140
        return CGSize.init(width: width, height: height)
        
    }
}
