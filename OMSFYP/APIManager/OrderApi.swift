//
//  OrderApi.swift
//  OMSFYP
//
//  Created by mac on 19/06/2021.
//
import Foundation
import Alamofire
import SwiftyJSON






struct OrderUrls{
    
    
    static var baseURL = "http://10.211.55.3:1037/"
    
    static var imageBaseURL = ProductURLS.baseURL + "Uploads/"
    
    static var AddOrder = ProductURLS.baseURL + "api/Order/PlaceOrder"
    
    static var DeleteOrder = ProductURLS.baseURL + "api/Order/DeleteOrder"
    
    static var UpdateStatus = ProductURLS.baseURL + "api/Order/ChangeStatus"
    
    static var OrderList = ProductURLS.baseURL + "api/Order/GetOrderList"
    
    static var OrderSingleList = ProductURLS.baseURL + "api/Order/GetSingleOrderList"
    
    static var changeStatus = baseURL + "api/Order/ChangeStatus"
    
    static var getID = baseURL + "api/Order/GetOrderId"
    
    static var AddDetails = baseURL + "api/OrderDetails/AddOrderDetails"
    
    static var OrderItemList = baseURL + "api/OrderDetails/GetOrderItemList"
}





class OrderApiManager{
    static func AddOrderDetails(data: OrderDetails, completion: @escaping(Bool?) -> ()){
        let regRequest: [String : Any] =  [
            "OID" : data.oid,
            "MID" : data.pid,
            "TOTAL" : data.total,
            "QUANTITY": data.quantity
        ]
        Utility.startLoading()
        AF.request(OrderUrls.AddDetails,
                   method: .post,
                   parameters: regRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? Bool {
                            
                            completion(jsonData as? Bool)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
    static func DeleteOrder(id:Int, completion: @escaping(Bool?) -> ()){
        let deleteRequest: [String: Any] = [
            "OID" : id,
        ]
        Utility.startLoading()
        AF.request(OrderUrls.DeleteOrder,
                   method: .post,
                   parameters: deleteRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    completion(false)
                    
                   }
    }
    static fileprivate func getOrderListwith(_ json: [[String: Any]]) -> [Order] {
        var allData = [Order]()
        var i = 0
        for record in json{
            var temp = Order()
            
            temp.oid =           record["OID"] as? Int
            temp.status =         record["OSTATUS"] as? String
            temp.uid =         record["UID"] as? Int
            temp.fromPrescription = record["FROMPRESCRIPTION"] as? Bool
            temp.grandtotal = record["OTOTAL"] as? Int
            temp.deliveryboyid = record["DBID"] as? Int
            temp.time = record["OTIME"] as? String
            allData.append(temp)
            i = i + 1
        }
        return allData
    }
    static func getOrderList(dsadsa: String, completion: @escaping([Order]) -> ()) {
        
        
        let OrderListRequest = [
            "MName" : dsadsa,
        ]
        //doesnt complete request
        Utility.startLoading()
        
        AF.request(OrderUrls.OrderList,
                   method: .post,
                   parameters: OrderListRequest,
                   encoder: JSONParameterEncoder.default).responseJSON{ response in
                    print("Response:")
                    Utility.stopLoading()
                    print(response.result)
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let user = self.getOrderListwith(jsonData)
                            completion(user)
                        } else {
                            
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        
                    }
                    
                   }
        
    }
    static func getOrderSingleList(ID: Int, completion: @escaping([Order]) -> ()) {
        
        
        let OrderListRequest = [
            "UID" : ID,
        ]
        //doesnt complete request
        Utility.startLoading()
        
        AF.request(OrderUrls.OrderSingleList,
                   method: .post,
                   parameters: OrderListRequest,
                   encoder: JSONParameterEncoder.default).responseJSON{ response in
                    print("Response:")
                    Utility.stopLoading()
                    print(response.result)
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let user = self.getOrderListwith(jsonData)
                            completion(user)
                        } else {
                            
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        
                    }
                    
                   }
        
    }
    static fileprivate func getOrderItemListwith(_ json: [[String: Any]]) -> [OrderDetails] {
        var allData = [OrderDetails]()
        var i = 0
        for record in json{
            var temp = OrderDetails()
            
            temp.oid =           record["OID"] as? Int
            temp.pid =         record["MID"] as? Int
            temp.quantity =         record["QUANTITY"] as? Int
            temp.total = record["TOTAL"] as? Int
            
            allData.append(temp)
            i = i + 1
        }
        return allData
    }
    static func getOrderItemList(ID: Int,single: Bool, completion: @escaping([OrderDetails]) -> ()) {
        var OrderListRequest = Parameters()
        if(single == true)
        {
            OrderListRequest = [
                "OID" : ID,
            ]
        }
        else{
            OrderListRequest = [
                "ALL" : "ALL"
            ]
        }
        
        //doesnt complete request
        Utility.startLoading()
        
        AF.request(OrderUrls.OrderItemList,
                   method: .post,
                   parameters: OrderListRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    print("Response:")
                    Utility.stopLoading()
                    print(response.result)
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let user = self.getOrderItemListwith(jsonData)
                            completion(user)
                        } else {
                            
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        
                    }
                    
                   }
        
    }
    static func getOrderWith(_ json: [String : Any]) -> Order  {
        var temp: Order = Order()
        temp.oid =           json["OID"] as? Int
        temp.status =         json["OSTATUS"] as? String
        temp.uid =         json["UID"] as? Int
        temp.fromPrescription = json["FROMPRESCRIPTION"] as? Bool
        temp.grandtotal = json["OTOTAL"] as? Int
        temp.deliveryboyid = json["DBID"] as? Int
        //temp.Address = json["Address"] as? String
        
        return temp
    }
    static func ChangeStatus(id:Int,status: String, completion: @escaping(Order?) -> ()){
        let changeRequest: [String: Any] = [
            "OID" : id,
            "OSTATUS" : status
        ]
        Utility.startLoading()
        AF.request(OrderUrls.changeStatus,
                   method: .post,
                   parameters: changeRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [String: Any] {
                            let user = self.getOrderWith(jsonData)
                            completion(user)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
    static func AddOrder(Data:Order, completion: @escaping(Bool?) -> ()){
        let regRequest: [String : Any] =  [
            "UID" : Data.uid,
            "DBID" : Data.deliveryboyid,
            //"OADDRESS"  : Data.Address,
            "OSTATUS" : Data.status,
            "OTOTAL": Data.grandtotal,
            "FROMPRESCRIPTION": false,
        ]
        Utility.startLoading()
        AF.request(OrderUrls.AddOrder,
                   method: .post,
                   parameters: regRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? Bool {
                            
                            completion(jsonData as? Bool)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
    static func GetOrderId(Data:Int, completion: @escaping(Int?) -> ()){
        let regRequest: [String : Any] =  [
            "UID" : Data,
        ]
        Utility.startLoading()
        AF.request(OrderUrls.getID,
                   method: .post,
                   parameters: regRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? Int {
                            
                            completion(jsonData as? Int)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
}
