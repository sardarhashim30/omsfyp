//
//  ImageApi.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//import Alamofire
import SwiftyJSON
import Foundation
import Alamofire
struct ImageURLS{
    static var upload = "http://10.211.55.3:1037/api/Image/UploadFiles"
    
}
class ImageAPIManager{
    static func UploadImage(imgData: Data, completion: @escaping(Bool?) -> ()){
        let parameters = ["name": "test"] //Optional for extra parameter
        Utility.startLoading()
        AF.upload(multipartFormData: { multiPart in
            Utility.stopLoading()
                for (key, value) in parameters {
                    if let temp = value as? String {
                        multiPart.append(temp.data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? Int {
                        multiPart.append("\(temp)".data(using: .utf8)!, withName: key)
                    }
                    if let temp = value as? NSArray {
                        temp.forEach({ element in
                            let keyObj = key + "[]"
                            if let string = element as? String {
                                multiPart.append(string.data(using: .utf8)!, withName: keyObj)
                            } else
                                if let num = element as? Int {
                                    let value = "\(num)"
                                    multiPart.append(value.data(using: .utf8)!, withName: keyObj)
                            }
                        })
                    }
                }
            multiPart.append(imgData, withName: "file", fileName: imgData.description.components(separatedBy: " ").first! + ".jpeg", mimeType: "image/png")
        }, to: ImageURLS.upload)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .responseJSON(completionHandler: { response in
                    //Do what ever you want to do with response
                        if let error = response.error {
                            print(error)
                        }
                        guard let data = response.value else {
                            return
                        }
                        
                })
    }
}
