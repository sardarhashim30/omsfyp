//
//  ProductApi.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//

import Foundation
import Alamofire
import SwiftyJSON
struct ProductURLS{
    static var baseURL = "http://10.211.55.3:1037/"
    static var imageBaseURL = "http://10.211.55.3:1037/Uploads/"
    static var getProduct = ProductURLS.baseURL + "api/Product/GetProductList"
    static var addProduct = ProductURLS.baseURL + "api/Product/AddProduct"
    static var delprod = ProductURLS.baseURL + "api/Product/DeleteProduct"
}
class ProductAPIManager {
    static func DeleteProduct(id:Int, completion: @escaping(Bool?) -> ()){
        let deleteRequest: [String: Any] = [
            "MID" : id,
        ]
        Utility.startLoading()
        AF.request(ProductURLS.delprod,
                   method: .post,
                   parameters: deleteRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    completion(false)
                    
                   }
    }
    static fileprivate func getProdList(_ json: [[String: Any]]) -> [Product] {
        var allData = [Product]()
        var i = 0
        for record in json{
            var temp = Product()
            temp.id    = record["MID"] as? Int
            temp.categoryId         = record["MTYPE"]as? String
            temp.description     = record["MDESCRIPTION"]as? String
            temp.imageUrl1         = record["MIMG1"]as? String
            temp.imageUrl2  = record["MIMG2"]as? String
            temp.imageUrl3        = record["MIMG3"]as? String
            temp.name     = record["MNAME"] as? String
            temp.price         = record["MPRICE"] as? String
            temp.company = record["MCOMPANY"] as? String
            allData.append(temp)
            i = i + 1
        }
        return allData
    }
    static func getProductList(dsadsa: String, completion: @escaping([Product]) -> ()) {
        
        
        let asd = [
            "MName" : dsadsa,
        ]
        //doesnt complete request
        Utility.startLoading()
        AF.request(ProductURLS.getProduct,
                   method: .post,
                   parameters: asd,
                   encoder: JSONParameterEncoder.default).responseJSON{ response in
                    print("Response:")
                    Utility.stopLoading()
                    print(response.result)
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let user = self.getProdList(jsonData)
                            completion(user)
                        } else {
                            
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        
                    }
                    
                   }
        
    }
    static func newproduct(Data: Product, completion: @escaping(Bool?) -> ()){
        let newprodRequest: [String : Any] =  [
            "MNAME" : Data.name,
            "MTYPE" : Data.categoryId,
            "MCOMPANY"  : Data.company,
            "MQUANTITY": Data.quantity,
            "MIMG1": Data.imageUrl1,
            "MIMG2" : Data.imageUrl2,
            "MIMG3": Data.imageUrl3,
            "MPRICE": Data.price
        ]
        Utility.startLoading()
        AF.request(ProductURLS.addProduct,
                   method: .post,
                   parameters: newprodRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? Bool {
                            
                            completion(jsonData as? Bool)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
}
