//
//  CartApi.swift
//  OMSFYP
//
//  Created by mac on 18/06/2021.
//

import Foundation
import Alamofire
import SwiftyJSON
struct CartURLs{
    static var baseURL = "http://10.211.55.3:1037/"
    static var imageBaseURL = ProductURLS.baseURL + "Uploads/"
    static var AddCart = ProductURLS.baseURL + "api/Cart/AddCart"
    static var DelCart = ProductURLS.baseURL + "api/Cart/DeleteCart"
    static var UpdateCart = ProductURLS.baseURL + "api/Cart/UpdateCart"
    static var CartList = ProductURLS.baseURL + "api/Cart/GetCartList"
}
class CartApiManager{
    static func DeleteCart(id:Int, completion: @escaping(Bool?) -> ()){
        let deleteRequest: [String: Any] = [
            "CID" : id,
        ]
        Utility.startLoading()
        AF.request(CartURLs.DelCart,
                   method: .post,
                   parameters: deleteRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    completion(false)
                    
                   }
    }
    static func AddCart(Data:Cart, completion: @escaping(Bool?) -> ()){
        let regRequest: [String : Any] =  [
            "MID" : Data.pid,
            "CQUANTITY" : Data.quantity,
            "CTOTAL"  : Data.total,
            "UID": Data.uid,
            
        ]
        Utility.startLoading()
         AF.request(CartURLs.AddCart,
                   method: .post,
                   parameters: regRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? Bool {
                            
                            completion(jsonData as? Bool)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
    static fileprivate func getCartList(_ json: [[String: Any]]) -> [Cart] {
        var allData = [Cart]()
        var i = 0
        for record in json{
            var temp = Cart()
            temp.pid    = record["MID"] as? Int
            temp.cid    = record["CID"] as? Int
            temp.quantity    = record["CQUANTITY"] as? String
            temp.total = record["CTOTAL"] as? Int
            
            allData.append(temp)
            i = i + 1
        }
        return allData
    }
    static func GetCartList(userID: Int, completion: @escaping([Cart]) -> ()) {
        
        
        let asd = [
            "UID" : userID,
        ]
        //doesnt complete request
        Utility.startLoading()
        AF.request(CartURLs.CartList,
                   method: .post,
                   parameters: asd,
                   encoder: JSONParameterEncoder.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    print(response.result)
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let user = self.getCartList(jsonData)
                            completion(user)
                        } else {
                            
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        
                    }
                    
                   }
        
    }
}
