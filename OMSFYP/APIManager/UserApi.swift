//
//  UserApi.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//
import Alamofire
import SwiftyJSON
import Foundation
struct URLS{
    static var login = "http://10.211.55.3:1037/api/User/Login"
    static var register = "http://10.211.55.3:1037/api/User/AddUser"
    static var getlist = "http://10.211.55.3:1037/api/User/getUserList"
    static var deleteuser = "http://10.211.55.3:1037/api/User/DeleteUser"
    static var getuser = "http://10.211.55.3:1037/api/User/GetUser"
    
}
class UserAPIManager{
    static func getUserWith(_ json: [String : Any]) -> User  {
        var temp: User = User()
        temp.id =           json["UID"] as? Int
        temp.name =         json["UNAME"] as? String
        temp.role =         json["UTYPE"] as? String
        temp.dob =          json["UAGE"] as? String
        temp.email =        json["UEMAIL"] as? String
        temp.gender =       json["UGENDER"] as? String
        temp.img =          json["UIMAGE"] as? String
        temp.address1 =     json["UADDRESS1"] as? String
        temp.address2 =     json["UADDRESS2"] as? String
        temp.password =     json["UPASSWORD"] as? String
        temp.phone =        json["UCONTACT"] as? String
        
        return temp
    }
    static func getSingleUser(UID:Int, completion: @escaping(User?) -> ()) {
        
        
        
        let loginRequest = [
            "UID" : UID,
        ]
        Utility.startLoading()
        AF.request(URLS.getuser,
                   method: .post,
                   parameters: loginRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    print(response.result)
                    
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [String: Any] {
                            let user = self.getUserWith(jsonData)
                            completion(user)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
        
    }
    static func loginValidator(email:String, password: String , completion: @escaping(User?) -> ()) {
        
        
        
        let loginRequest = [
            "UEmail" : email,
            "UPassword" : password
        ]
        Utility.startLoading()
        AF.request(URLS.login,
                   method: .post,
                   parameters: loginRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    Utility.stopLoading()
                    print("Response:")
                    print(response.result)
                    
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [String: Any] {
                            let user = self.getUserWith(jsonData)
                            completion(user)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
        
    }
    static func DeleteUser(id:Int, completion: @escaping(Bool?) -> ()){
        let deleteRequest: [String: Any] = [
            "UID" : id,
        ]
        Utility.startLoading()
        AF.request(URLS.deleteuser,
                   method: .post,
                   parameters: deleteRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    print("Response:")
                    Utility.stopLoading()

                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? Bool {
                            
                            completion(jsonData as? Bool)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
    static func registration(User:User, completion: @escaping(Bool?) -> ()){
        let regRequest: [String : Any] =  [
            "UNAME" : User.name,
            "UTYPE" : User.role,
            "UAGE"  : User.dob,
            "UEMAIL": User.email,
            "UGENDER": User.gender,
            "UIMAGE" : User.img,
            "UADDRESS1": User.address1,
            "UADDRESS2": User.address2,
            "UPASSWORD": User.password,
            "UCONTACT": User.phone
        ]
        Utility.startLoading()
        AF.request(URLS.register,
                   method: .post,
                   parameters: regRequest,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    print("Response:")
                    Utility.stopLoading()

                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? Bool {
                            
                            completion(jsonData as? Bool)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
    static fileprivate func getUserwith(_ json: [[String: Any]]) -> [User] {
        var allData = [User]()
        var i = 0
        for record in json{
            var temp = User()
            temp.id    = record["UID"] as? Int
            temp.address1         = record["UADDRESS1"]as? String
            temp.address2     = record["UADDRESS2"]as? String
            temp.img         = record["UIMAGE"]as? String
            temp.gender  = record["UGENDER"]as? String
            temp.password        = record["UPASSWORD"]as? String
            temp.name     = record["UNAME"] as? String
            temp.role         = record["UTYPE"] as? String
            temp.phone = record["UCONTACT"] as? String
            allData.append(temp)
            i = i + 1
        }
        return allData
    }
    static func getUserList(dsadsa: String, completion: @escaping([User]) -> ()) {
        
        
        let asd = [
            "MName" : dsadsa,
        ]
        //doesnt complete request
        Utility.startLoading()

        AF.request(URLS.getlist,
                   method: .post,
                   parameters: asd,
                   encoder: JSONParameterEncoder.default).responseJSON{ response in
                    print("Response:")
                    Utility.stopLoading()
                    print(response.result)
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let user = self.getUserwith(jsonData)
                            completion(user)
                        } else {
                            
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        
                    }
                    
                   }
        
    }
}
