//
//  Appstatemanager.swift
//  OMSFYP
//
//  Created by mac on 17/06/2021.
//

import Foundation



class Appstatemanager : NSObject {
    
    static var shared = Appstatemanager()
    
    
    var loggedInUser: User! {
        get{
            User.get()
        } set {
            User.save(newValue)
        }
    }
    
    var isLoggedIn: Bool! {
        get { return UserDefaults.standard.bool(forKey: "isLoggedIn") }
        set { UserDefaults.standard.set(newValue, forKey: "isLoggedIn") }
    }
    
    
}

