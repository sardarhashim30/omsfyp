//
//  Utility.swift
//  OMSFYP
//
//  Created by shh on 17/06/2021.
//

import Foundation
import UIKit
class Utility{
    static func displayAlertMessage(userMessage: String, okTapped: @escaping() -> (),VC: UIViewController) {

        let myAlert = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title:"Ok", style: UIAlertAction.Style.default) {
            action in
            VC.dismiss(animated: true, completion:{
                okTapped()
            })
        }

        myAlert.addAction(okAction);

        VC.present(myAlert, animated: true, completion: nil)
    }
    
    static func displayAlertMessage(userMessage: String, okTapped: @escaping() -> (),button2Tapped: @escaping() -> (),VC: UIViewController) {

        let myAlert = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title:"Cancel", style: UIAlertAction.Style.default) {
            action in
            VC.dismiss(animated: true, completion:{
                okTapped()
            })
        }
        let button2 = UIAlertAction(title:"Login", style: UIAlertAction.Style.default) {
            action in
            VC.dismiss(animated: true, completion:{
                button2Tapped()
            })
        }

        myAlert.addAction(okAction);
        myAlert.addAction(button2);

        VC.present(myAlert, animated: true, completion: nil)
    }

    
    static func startLoading() {
        let activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 30, width: 150, height: 100))
        activityIndicator.backgroundColor = UIColor.clear
        activityIndicator.layer.cornerRadius = 6
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .whiteLarge
        activityIndicator.startAnimating()
        
        
        
        
        
        
        let strLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 30))
        let messageFrame = UIView()
        strLabel.text = NSLocalizedString("Loading", comment: "")
        strLabel.textColor = UIColor.white
        strLabel.textAlignment = .center
        messageFrame.layer.cornerRadius = 15
        messageFrame.backgroundColor = UIColor(white: 0, alpha: 0.7)
        messageFrame.frame = CGRect(x: 0, y: 0, width: 150, height: 130)
        var viewCenter = (Utility.topViewController()?.view.center)!
        viewCenter.y = (viewCenter.y - messageFrame.frame.size.height)
        messageFrame.center = viewCenter
        
        
        messageFrame.tag = 100
        
        messageFrame.addSubview(activityIndicator)
        messageFrame.addSubview(strLabel)
        
        
        
        for subview in (Utility.topViewController()?.view.subviews)! {
            if subview.tag == 100 {
                return
            }
        }
//        Utility.topViewController()?.view.isUserInteractionEnabled = false
        
        Utility.topViewController()?.view.addSubview(messageFrame)
    }
    
    static func stopLoading() {
        let activityIndicator = Utility.topViewController()?.view.viewWithTag(100)
        DispatchQueue.main.async {
            
            activityIndicator?.removeFromSuperview()
//            Utility.topViewController()?.view.isUserInteractionEnabled = true
        }
        
        
    }
    
    
    
    static func topViewController() -> UIViewController? {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }

}
